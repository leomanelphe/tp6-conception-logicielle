from fastapi import FastAPI
from pydantic import BaseModel


class Mot(BaseModel):
    id: int
    caracteres: str

    def __init__(self, id, caracteres):
        self.id = id
        self.caracteres = caracteres


mot1 = Mot(id=1, caracteres="rouge")
mot2 = Mot(id=2, caracteres="vert")
mot3 = Mot(id=3, caracteres="bleu")

mots = [mot1, mot2, mot3]

app = FastAPI()


@app.get("/")
def read_root():
    return {"bonjour"}


@app.get("/mots")
def get_all_words():
    return mots


@app.post("/mot/")
async def create_mot(mot: Mot):
    mots.append(mot)
    return mot
